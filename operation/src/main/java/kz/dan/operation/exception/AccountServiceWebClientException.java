package kz.dan.operation.exception;

public class AccountServiceWebClientException extends RuntimeException{
    public AccountServiceWebClientException() {
        super("Internal server error");
    }
}

package kz.dan.operation.exception;

public class AccountNotBelongToUser extends RuntimeException {
    public AccountNotBelongToUser(String accountId) {
        super("The account " + accountId + " does not belong to the user");
    }
}

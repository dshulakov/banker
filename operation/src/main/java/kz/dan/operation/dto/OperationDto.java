package kz.dan.operation.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kz.dan.operation.constants.Direction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OperationDto {
    private String user;
    private LocalDateTime date;
    private Direction direction;
    private String account;
    private long sum;
}

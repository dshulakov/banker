package kz.dan.operation.dto;

import lombok.Data;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
public class PageDto {
    @PositiveOrZero
    private int pageNumber;

    @Positive
    private int pageSize = 20;

    private String sortBy = "date";

    private Sort.Direction direction = Sort.Direction.ASC;
}

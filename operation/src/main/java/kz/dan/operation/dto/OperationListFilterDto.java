package kz.dan.operation.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.dan.operation.constants.Direction;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class OperationListFilterDto {
    @JsonIgnore
    private String user;
    private LocalDateTime from;
    private LocalDateTime to;
    private String account;
    private Direction direction;
}

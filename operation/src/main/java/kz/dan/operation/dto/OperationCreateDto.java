package kz.dan.operation.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.dan.operation.constants.Direction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OperationCreateDto {
    @JsonIgnore
    private String user;
    private LocalDateTime date;
    private Direction direction;
    private String account;

    @Min(0)
    private long sum;
}

package kz.dan.operation.repository;

import kz.dan.operation.constants.Direction;
import kz.dan.operation.entity.Operation;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.UUID;

public interface OperationRepository extends JpaRepository<Operation, UUID> {

    @Query(
            value = "from Operation " +
                    "where user = :user " +
                    "and date between :from and :to " +
                    "and (:account is null or account = :account) " +
                    "and (:direction is null or direction = :direction)"
            ,
            countQuery = "select count(id) from Operation " +
                    "where user = :user " +
                    "and date between :from and :to " +
                    "and (:account is null or account = :account) " +
                    "and (:direction is null or direction = :direction)"
    )
    PageImpl<Operation> getOperations(@Param("user") String user,
                                      @Param("from") LocalDateTime from,
                                      @Param("to") LocalDateTime to,
                                      @Param("account") String account,
                                      @Param("direction") Direction direction,
                                      Pageable pageable);
}

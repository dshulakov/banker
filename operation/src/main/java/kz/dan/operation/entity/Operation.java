package kz.dan.operation.entity;

import kz.dan.operation.constants.Direction;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@Table(name = "operation")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Operation extends AbstractEntity {
    @Column(name = "username", nullable = false, length = 36)
    @NotBlank
    private String user;

    @Column(name = "operation_date", nullable = false)
    private LocalDateTime date;

    @Column(name = "direction", nullable = false)
    @Enumerated(EnumType.STRING)
    private Direction direction;

    @Column(name = "account", nullable = false, length = 36)
    @NotBlank
    private String account;

    @Column(name = "sum")
    private long sum;

}

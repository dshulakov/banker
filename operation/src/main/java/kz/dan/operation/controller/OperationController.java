package kz.dan.operation.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import kz.dan.operation.constants.Direction;
import kz.dan.operation.dto.OperationCreateDto;
import kz.dan.operation.dto.OperationDto;
import kz.dan.operation.dto.OperationListFilterDto;
import kz.dan.operation.dto.PageDto;
import kz.dan.operation.entity.Operation;
import kz.dan.operation.repository.OperationRepository;
import kz.dan.operation.service.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping()
public class OperationController {
    private final OperationRepository operationRepository;
    private final OperationService operationService;

    @PostMapping("/add")
    public ResponseEntity<?> add(@Valid @RequestBody OperationCreateDto operationCreateDto) {
        OperationDto operationDto = operationService.create(operationCreateDto);
        return new ResponseEntity<>(operationDto, HttpStatus.CREATED);
    }

    @PostMapping("/add-list")
    public ResponseEntity<?> addList(@Valid @RequestBody List<OperationCreateDto> operationCreateDto) {
        operationService.create(operationCreateDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<OperationDto>> operationsList(
            @RequestParam(name = "from")  @DateTimeFormat(pattern= "yyyy-MM-dd") Date from,
            @RequestParam(name = "to") @DateTimeFormat(pattern= "yyyy-MM-dd") Date to,
            @RequestParam(name = "account", required = false) String account,
            @RequestParam(name = "direction", required = false) Direction direction,
            PageDto pageDto) {

        OperationListFilterDto filter = OperationListFilterDto.builder()
                .from(from.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                .to(to.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                .account(account)
                .direction(direction)
                .build();
        PageRequest pageRequest = PageRequest.of(pageDto.getPageNumber(), pageDto.getPageSize(),
                Sort.by(pageDto.getDirection(), pageDto.getSortBy()));

        Page<OperationDto> operationDtoPage = operationService.getAll(filter, pageRequest);
        return new ResponseEntity<>(operationDtoPage, HttpStatus.OK);
    }
}

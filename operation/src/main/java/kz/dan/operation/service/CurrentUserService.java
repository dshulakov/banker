package kz.dan.operation.service;

import kz.dan.operation.constants.Roles;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserService {
    public String getName() {
        return getAuthentication().getName();
    }

    public boolean isAdmin() {
        return hasRole(Roles.ADMIN);
    }

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    private boolean hasRole(Roles role) {
        Authentication authentication = getAuthentication();
        return authentication.getAuthorities().stream()
                .anyMatch(f -> f.getAuthority().equals("ROLE_" + role));
    }
}

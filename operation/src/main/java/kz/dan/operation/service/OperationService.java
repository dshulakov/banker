package kz.dan.operation.service;

import kz.dan.operation.dto.OperationCreateDto;
import kz.dan.operation.dto.OperationDto;
import kz.dan.operation.dto.OperationListFilterDto;
import kz.dan.operation.entity.Operation;
import kz.dan.operation.exception.AccountNotBelongToUser;
import kz.dan.operation.mapper.OperationMapper;
import kz.dan.operation.repository.OperationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OperationService {
    private final OperationRepository operationRepository;
    private final OperationMapper operationMapper;
    private final CurrentUserService currentUserService;
    private final AccountService accountService;

    @Transactional
    public OperationDto create(OperationCreateDto operationCreateDto) {
        validateOperationCreateDto(List.of(operationCreateDto));
        operationCreateDto.setUser(currentUserService.getName());
        Operation operation = operationMapper.mapOperationCreateDtoToOperation(operationCreateDto);
        operationRepository.save(operation);
        return operationMapper.mapOperationToOperationDto(operation);
    }

    @Transactional
    public void create(List<OperationCreateDto> operationCreateDtoList) {
        validateOperationCreateDto(operationCreateDtoList);
        String user = currentUserService.getName();
        operationCreateDtoList.forEach(o -> o.setUser(user));
        List<Operation> operations = operationMapper.mapListOperationCreateDtoToListOperation(operationCreateDtoList);
        operationRepository.saveAll(operations);
    }

    private void validateOperationCreateDto(List<OperationCreateDto> operationCreateDtoList) {
        List<String> userAccounts = accountService
                .getUserAccounts(currentUserService.getName())
                .getAccounts();

        Set<String> operationAccounts = operationCreateDtoList.stream()
                .map(OperationCreateDto::getAccount)
                .collect(Collectors.toSet());

        operationAccounts.forEach(operationAccount -> {
            userAccounts.stream()
                    .filter(userAccount -> userAccount.equals(operationAccount))
                    .findAny()
                    .orElseThrow(() -> new AccountNotBelongToUser(operationAccount)
                    );
        });
    }

    public Page<OperationDto> getAll(OperationListFilterDto filter, Pageable pageable) {
        filter.setUser(currentUserService.getName());
        PageImpl<Operation> operations = operationRepository.getOperations(
                filter.getUser(),
                filter.getFrom(),
                filter.getTo(),
                filter.getAccount(),
                filter.getDirection(),
                pageable);

        return  operations.map(operationMapper::mapOperationToOperationDto);
}
}

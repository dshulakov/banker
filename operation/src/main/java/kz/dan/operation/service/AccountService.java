package kz.dan.operation.service;

import kz.dan.operation.dto.UserAccountsDto;
import kz.dan.operation.exception.AccountServiceWebClientException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {
    private final WebClient webClient;

    @Value("${service.account}")
    private String accountServiceUri;

    public UserAccountsDto getUserAccounts(String user) {
        try {
            return getUserAccountsDto(user);
        } catch (WebClientRequestException | WebClientResponseException ex) {
            log.error(ex.getMessage());
            throw new AccountServiceWebClientException();
        }
    }

    private UserAccountsDto getUserAccountsDto(String user) {
        return webClient.get()
                .uri(accountServiceUri, uriBuilder -> uriBuilder
                        .path("/user")
                        .queryParam("id", user)
                        .build())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(UserAccountsDto.class)
                .block();
    }
}

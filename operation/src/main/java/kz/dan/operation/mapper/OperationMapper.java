package kz.dan.operation.mapper;

import kz.dan.operation.dto.OperationCreateDto;
import kz.dan.operation.dto.OperationDto;
import kz.dan.operation.entity.Operation;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OperationMapper extends ModelMapper {
    public OperationMapper() {
        super();
        operationCreateDtoToAccountTypeMap();
        operationToOperationDtoTypeMap();
    }

    public Operation mapOperationCreateDtoToOperation(OperationCreateDto operationCreateDto) {
        return this.map(operationCreateDto, Operation.class);
    }

    public List<Operation> mapListOperationCreateDtoToListOperation(List<OperationCreateDto> operationCreateDtoList) {
        return this.map(operationCreateDtoList, new TypeToken<List<Operation>>(){}.getType());
    }

    public OperationDto mapOperationToOperationDto(Operation operation) {
        return this.map(operation, OperationDto.class);
    }

    private void operationCreateDtoToAccountTypeMap() {
        TypeMap<OperationCreateDto, Operation> typeMap = this.createTypeMap(OperationCreateDto.class, Operation.class);
        typeMap.addMapping(OperationCreateDto::getUser, Operation::setUser);
        typeMap.addMapping(OperationCreateDto::getDate, Operation::setDate);
        typeMap.addMapping(OperationCreateDto::getDirection, Operation::setDirection);
        typeMap.addMapping(OperationCreateDto::getAccount, Operation::setAccount);
        typeMap.addMapping(OperationCreateDto::getSum, Operation::setSum);
    }

    private void operationToOperationDtoTypeMap() {
        TypeMap<Operation, OperationDto> typeMap = this.createTypeMap(Operation.class, OperationDto.class);
        typeMap.addMapping(Operation::getUser, OperationDto::setUser);
        typeMap.addMapping(Operation::getDate, OperationDto::setDate);
        typeMap.addMapping(Operation::getDirection, OperationDto::setDirection);
        typeMap.addMapping(Operation::getAccount, OperationDto::setAccount);
        typeMap.addMapping(Operation::getSum, OperationDto::setSum);
    }



}

package kz.dan.auth.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;

public class CustomErrorDecoder implements ErrorDecoder {

    @SneakyThrows
    @Override
    public Exception decode(String methodKey, Response response) {
        HttpStatus responseStatus = HttpStatus.valueOf(response.status());

        if (responseStatus == HttpStatus.UNAUTHORIZED) {
            return new UnauthorizedException();
        }

        if (responseStatus == HttpStatus.BAD_REQUEST) {
            Response.Body responseBody = response.body();
            if (responseBody == null) {
                return new RefreshTokenException("Error while processing refresh token");
            }

            try (InputStream bodyIs = response.body().asInputStream()) {
                ObjectMapper mapper = new ObjectMapper();
                OAuth2ServerExceptionMessage exceptionMessage = mapper.readValue(bodyIs, OAuth2ServerExceptionMessage.class);
                return new RefreshTokenException(exceptionMessage.getErrorDescription());
            } catch (IOException e) {
                return new Exception(e.getMessage());
            }
        }

       return new OAuth2ServerException();
    }
}
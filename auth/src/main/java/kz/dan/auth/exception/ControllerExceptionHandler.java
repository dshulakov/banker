package kz.dan.auth.exception;

import kz.dan.auth.dto.ExceptionDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    @ExceptionHandler(value = UnauthorizedException.class)
    protected ResponseEntity<ExceptionDto> handle(UnauthorizedException ex) {
        log.error("UNAUTHORIZED");
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = RefreshTokenException.class)
    protected ResponseEntity<ExceptionDto> handle(RefreshTokenException ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(buildExceptionDto(ex), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OAuth2ServerException.class)
    protected ResponseEntity<ExceptionDto> handle(OAuth2ServerException ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(buildExceptionDto(ex), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity<ExceptionDto> handle(Exception ex) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(buildExceptionDto(ex), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ExceptionDto buildExceptionDto(Exception ex) {
        return ExceptionDto.builder()
                .message(ex.getMessage())
                .build();

    }
}
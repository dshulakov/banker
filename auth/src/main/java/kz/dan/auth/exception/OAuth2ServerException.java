package kz.dan.auth.exception;

public class OAuth2ServerException extends RuntimeException{
    public OAuth2ServerException() {
        super("server error");
    }
}

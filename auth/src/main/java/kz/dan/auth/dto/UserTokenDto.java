package kz.dan.auth.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserTokenDto {
    private String token;
    private String refreshToken;
}

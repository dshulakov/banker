package kz.dan.auth.dto;

import kz.dan.auth.enums.GrandType;
import lombok.Data;

@Data
public class GetTokenDto {
    public GetTokenDto(String client_id, String username, String password) {
        this.client_id = client_id;
        this.username = username;
        this.password = password;
        this.grant_type = GrandType.PASSWORD.get();
    }

    private String client_id;
    private String username;
    private String password;
    private String grant_type;
}

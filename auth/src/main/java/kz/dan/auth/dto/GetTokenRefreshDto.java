package kz.dan.auth.dto;

import kz.dan.auth.enums.GrandType;
import lombok.Data;

@Data
public class GetTokenRefreshDto {
    public GetTokenRefreshDto(String clientId, String refresh_token) {
        this.client_id = clientId;
        this.refresh_token = refresh_token;
        this.grant_type = GrandType.REFRESH_TOKEN.get();
    }

    private String grant_type;
    private String client_id;
    private String refresh_token;
}

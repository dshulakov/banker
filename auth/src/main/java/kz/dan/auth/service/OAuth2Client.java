package kz.dan.auth.service;

import kz.dan.auth.config.FormFeignEncoderConfig;
import kz.dan.auth.dto.GetTokenDto;
import kz.dan.auth.dto.GetTokenRefreshDto;
import kz.dan.auth.dto.TokenDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "auth-service", url = "${oauth2.token-url}", configuration = FormFeignEncoderConfig.class)
public interface OAuth2Client {
    @RequestMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    TokenDto getToken(@RequestBody GetTokenDto getTokenDto);

    @RequestMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    TokenDto getToken(@RequestBody GetTokenRefreshDto getTokenRefreshDto);
}

package kz.dan.auth.service;

import kz.dan.auth.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TokenService {
    private final OAuth2Client oauth2Client;
    @Value("${oauth2.client-id}")
    private String clientId;

    public UserTokenDto getToken(UserLoginDto userLoginDto) {
        GetTokenDto getTokenDto = new GetTokenDto(
                clientId,
                userLoginDto.getUsername(),
                userLoginDto.getPassword());
        TokenDto tokenDto = oauth2Client.getToken(getTokenDto);
        return mapTokenDtoToUserTokenDto(tokenDto);
    }

    public UserTokenDto refreshToken(RefreshTokenDto refreshTokenDto) {
        GetTokenRefreshDto getTokenRefreshDto = new GetTokenRefreshDto(
                clientId,
                refreshTokenDto.getRefreshToken()
        );
        TokenDto tokenDto = oauth2Client.getToken(getTokenRefreshDto);
        return mapTokenDtoToUserTokenDto(tokenDto);
    }

    private UserTokenDto mapTokenDtoToUserTokenDto(TokenDto tokenDto) {
        return UserTokenDto.builder()
                .token(tokenDto.getAccessToken())
                .refreshToken(tokenDto.getRefreshToken())
                .build();
    }
}

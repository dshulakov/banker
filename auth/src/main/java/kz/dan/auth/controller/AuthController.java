package kz.dan.auth.controller;

import kz.dan.auth.dto.RefreshTokenDto;
import kz.dan.auth.dto.UserLoginDto;
import kz.dan.auth.dto.UserTokenDto;
import kz.dan.auth.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController {
    private final TokenService tokenService;

    @PostMapping("/token")
    public UserTokenDto token(@RequestBody UserLoginDto userLoginDto) {
        return tokenService.getToken(userLoginDto);
    }

    @PostMapping("/refresh")
    public UserTokenDto refresh(@RequestBody RefreshTokenDto refreshTokenDto) {
        return tokenService.refreshToken(refreshTokenDto);
    }
}

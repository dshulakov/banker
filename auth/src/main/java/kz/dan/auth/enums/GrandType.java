package kz.dan.auth.enums;

public enum GrandType {
    PASSWORD("password"),
    REFRESH_TOKEN("refresh_token");

    private String type;

    GrandType(String envUrl) {
        this.type = envUrl;
    }

    public String get() {
        return type;
    }
}

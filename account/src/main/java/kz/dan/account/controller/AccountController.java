package kz.dan.account.controller;

import kz.dan.account.dto.AccountCreateDto;
import kz.dan.account.dto.AccountDto;
import kz.dan.account.dto.AccountUpdateDto;
import kz.dan.account.dto.UserAccountsDto;
import kz.dan.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping()
public class AccountController {
    private final AccountService accountService;

    @GetMapping
    public ResponseEntity<List<AccountDto>> get() {
        return new ResponseEntity<>(accountService.getAll(), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<AccountDto> create(@Valid @RequestBody AccountCreateDto accountCreateDto) {
        AccountDto accountDto = accountService.create(accountCreateDto);
        return new ResponseEntity<>(accountDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {
        accountService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable String id, @Valid @RequestBody AccountUpdateDto accountUpdateDto) {
        AccountDto accountDto = accountService.update(id, accountUpdateDto);
        return new ResponseEntity<>(accountDto, HttpStatus.OK);
    }

    @GetMapping("/user")
    public ResponseEntity<?> getByOwner(@RequestParam(name = "id") String userId) {
        UserAccountsDto userAccountsDto = accountService.getUserAccounts(userId);
        return new ResponseEntity<>(userAccountsDto, HttpStatus.OK);
    }
}


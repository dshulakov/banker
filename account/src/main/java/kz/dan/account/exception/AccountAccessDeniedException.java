package kz.dan.account.exception;

public class AccountAccessDeniedException extends RuntimeException{
    public AccountAccessDeniedException(String id) {
        super("Account access denied! Account id: " + id);
    }
}

package kz.dan.account.exception;

public class IllegalUuidException extends RuntimeException{
    public IllegalUuidException(String uuid) {
        super("Illegal uuid format: " + uuid);
    }
}

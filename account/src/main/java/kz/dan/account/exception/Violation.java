package kz.dan.account.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Violation {
    private String field;
    private String message;
}

package kz.dan.account.mapper;

import kz.dan.account.dto.AccountCreateDto;
import kz.dan.account.dto.AccountDto;
import kz.dan.account.entity.Account;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountMapper extends ModelMapper {
    public AccountMapper() {
        super();
        accountCreateDtoToAccountTypeMap();
        accountToAccountDtoTypeMap();
    }

    public Account mapAccountCreateDtoToAccount(AccountCreateDto accountCreateDto) {
        return this.map(accountCreateDto, Account.class);
    }

    public AccountDto mapAccountToAccountDto(Account account) {
        return this.map(account, AccountDto.class);
    }

    public List<AccountDto> mapListAccountToListAccountDto(List<Account> accounts) {
        return this.map(accounts, new TypeToken<List<AccountDto>>(){}.getType());
    }

    private void accountCreateDtoToAccountTypeMap() {
        TypeMap<AccountCreateDto, Account> typeMap = this.createTypeMap(AccountCreateDto.class, Account.class);
        typeMap.addMapping(AccountCreateDto::getName, Account::setName);
        typeMap.addMapping(AccountCreateDto::getNumber, Account::setNumber);
        typeMap.addMapping(AccountCreateDto::getOwner, Account::setOwner);
    }

    private void accountToAccountDtoTypeMap() {
        TypeMap<Account, AccountDto> typeMap = this.createTypeMap(Account.class, AccountDto.class);
        typeMap.addMapping(Account::getId, AccountDto::setId);
        typeMap.addMapping(Account::getName, AccountDto::setName);
        typeMap.addMapping(Account::getOwner, AccountDto::setOwner);
    }

}

package kz.dan.account.util;

import kz.dan.account.exception.IllegalUuidException;

import java.util.UUID;

public class UuidUtil {
    public static UUID uuidFromString(String id) {
        try {
            return UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            throw new IllegalUuidException(id);
        }
    }
}

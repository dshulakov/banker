package kz.dan.account.constants;

public enum Roles {
    ADMIN, USER, INTERNAL_SERVICE, MONITORING;
}

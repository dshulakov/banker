package kz.dan.account.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "account")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account extends AbstractEntity {
    @Column(name = "owner", nullable = false, length = 36)
    @NotBlank
    private String owner;

    @Column(name = "name", nullable = false, length = 150)
    private String name;

    @Column(name = "number", length = 20)
    private String number;
}

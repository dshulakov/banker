package kz.dan.account.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreateDto {
    @JsonIgnore
    private String owner;
    @NotBlank
    @Size(min = 1, max = 150)
    private String name;
    @Size(max = 20)
    private String number;
}

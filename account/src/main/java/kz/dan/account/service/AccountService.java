package kz.dan.account.service;

import kz.dan.account.dto.AccountCreateDto;
import kz.dan.account.dto.AccountDto;
import kz.dan.account.dto.AccountUpdateDto;
import kz.dan.account.dto.UserAccountsDto;
import kz.dan.account.entity.Account;
import kz.dan.account.exception.AccountAccessDeniedException;
import kz.dan.account.exception.AccountNotFoundException;
import kz.dan.account.mapper.AccountMapper;
import kz.dan.account.repository.AccountRepository;
import kz.dan.account.util.UuidUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final CurrentUserService currentUserService;

    public List<AccountDto> getAll() {
        List<Account> accounts;
        if (currentUserService.isAdmin()) {
            accounts = accountRepository.findAll();
        } else {
            accounts = accountRepository.findAllByOwner(currentUserService.getName());
        }
        return accountMapper.mapListAccountToListAccountDto(accounts);
    }

    public AccountDto create(AccountCreateDto accountCreateDto) {
        String currentUserName = currentUserService.getName();
        accountCreateDto.setOwner(currentUserName);
        Account account = accountMapper.mapAccountCreateDtoToAccount(accountCreateDto);
        accountRepository.save(account);
        return accountMapper.mapAccountToAccountDto(account);
    }

    public void delete(String id) {
        UUID uuid = UuidUtil.uuidFromString(id);
        Account account = getAccountById(uuid);
        accountRepository.delete(account);
    }

    public AccountDto update(String id, AccountUpdateDto accountUpdateDto) {
        UUID uuid = UuidUtil.uuidFromString(id);
        Account account = getAccountById(uuid);

        String currentUserName = currentUserService.getName();
        String owner = account.getOwner();

        if (!currentUserService.isAdmin() && !owner.equals(currentUserName)) {
            throw new AccountAccessDeniedException(id);
        }

        account.setName(accountUpdateDto.getName());
        account.setNumber(accountUpdateDto.getNumber());
        accountRepository.save(account);
        return accountMapper.mapAccountToAccountDto(account);
    }

    private Account getAccountById(UUID id) {
        return accountRepository.findById(id).orElseThrow(() -> new AccountNotFoundException(id.toString()));
    }

    public UserAccountsDto getUserAccounts(String userId) {
        List<Account> accounts = accountRepository.findAllByOwner(userId);
        List<String> listId = accounts.stream().map(a -> a.getId().toString()).collect(Collectors.toList());
        UserAccountsDto ownerAccountsDto = UserAccountsDto.builder()
                .user(userId)
                .accounts(listId)
                .build();
        return ownerAccountsDto;
    }
}

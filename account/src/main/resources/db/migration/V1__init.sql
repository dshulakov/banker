create table account
(
    id         uuid         not null,
    created_at timestamp,
    updated_at timestamp,
    name       varchar(150) not null,
    number     varchar(20),
    owner      varchar(36)  not null,
    primary key (id)
)
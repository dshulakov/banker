package kz.dan.account.AccountControllerTest;


import kz.dan.account.AccountAppIntegrationTest;
import kz.dan.account.KeyCloakToken;
import kz.dan.account.entity.Account;
import kz.dan.account.repository.AccountRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetAccountTest extends AccountAppIntegrationTest {
    private final String URL = "/";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @BeforeAll
    void beforeAll() {

        KeyCloakToken adminToken = adminToken();
        KeyCloakToken userToken = userToken();

        Account userAccount = Account.builder()
                .name("user name")
                .number("user number")
                .owner(String.valueOf(userToken.getPayload().get("sub")))
                .build();

        Account adminAccount = Account.builder()
                .name("admin name")
                .number("admin number")
                .owner(String.valueOf(adminToken.getPayload().get("sub")))
                .build();
        accountRepository.saveAll(List.of(userAccount, adminAccount));

    }

    @AfterAll
    void afterAll() {
        accountRepository.deleteAll();
    }

    @Test
    public void user_shouldStatusOk() throws Exception {
        KeyCloakToken token = userToken();
        mockMvc.perform(MockMvcRequestBuilders.get(URL)
                        .header("Authorization", "Bearer " + token.getAccessToken())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value("1"));
    }

    @Test
    public void admin_shouldStatusOk() throws Exception {
        KeyCloakToken token = adminToken();
        mockMvc.perform(MockMvcRequestBuilders.get(URL)
                        .header("Authorization", "Bearer " + token.getAccessToken())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value("2"));
    }

    @Test
    public void shouldStatusUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(URL)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

}

package kz.dan.account.AccountControllerTest;


import kz.dan.account.AccountAppIntegrationTest;
import kz.dan.account.KeyCloakToken;
import kz.dan.account.dto.AccountCreateDto;
import kz.dan.account.entity.Account;
import kz.dan.account.repository.AccountRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateAccountTest extends AccountAppIntegrationTest {
    private final String URL = "/add";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @AfterAll
    void afterAll() {
        accountRepository.deleteAll();
    }

    @Test
    public void shouldStatusCreated() throws Exception {
        AccountCreateDto accountCreateDto = AccountCreateDto.builder()
                .name("new name")
                .number("new number")
                .build();

        KeyCloakToken token = userToken();
        mockMvc.perform(MockMvcRequestBuilders.post(URL)
                        .header("Authorization", "Bearer " + token.getAccessToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(accountCreateDto)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldMethodArgumentNotValidException() throws Exception {
        AccountCreateDto accountCreateDto = AccountCreateDto.builder().build();

        KeyCloakToken token = userToken();
        mockMvc.perform(MockMvcRequestBuilders.post(URL)
                        .header("Authorization", "Bearer " + token.getAccessToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(accountCreateDto)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException));
    }

    @Test
    public void shouldStatusUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(URL)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

}

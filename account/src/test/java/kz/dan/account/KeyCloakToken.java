package kz.dan.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Base64;
import java.util.HashMap;

public class KeyCloakToken {
    private final String accessToken;
    private final HashMap<String,Object> payload;

    @JsonCreator
    KeyCloakToken(@JsonProperty("access_token") final String accessToken) throws JsonProcessingException {
        this.accessToken = accessToken;
        String payloadBase54 = accessToken.split("\\.")[1];
        byte[] decodedBytes = Base64.getDecoder().decode(payloadBase54);
        String payloadJson = new String(decodedBytes);
        ObjectMapper mapper = new ObjectMapper();
        this.payload = mapper.readValue(payloadJson, new TypeReference<>() {});
    }

    public String getAccessToken() {
        return accessToken;
    }

    public HashMap<String, Object> getPayload() {
        return payload;
    }
}

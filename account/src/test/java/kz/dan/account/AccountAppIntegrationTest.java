package kz.dan.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dasniko.testcontainers.keycloak.KeycloakContainer;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.junit.jupiter.Testcontainers;


import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;

@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AccountAppIntegrationTest {
    static final KeycloakContainer keycloak;
    protected static final String authServerUrl;

    static {
        keycloak = new KeycloakContainer().withRealmImportFile("keycloak/banker-realm.json");
        keycloak.start();
        authServerUrl = keycloak.getAuthServerUrl() + "/realms/banker-realm/protocol/openid-connect/token";
    }

    @DynamicPropertySource
    static void registerResourceServerIssuerProperty(DynamicPropertyRegistry registry) {
        registry.add("spring.security.oauth2.resourceserver.jwt.jwk-set-uri",
                () -> keycloak.getAuthServerUrl() + "/realms/banker-realm/protocol/openid-connect/certs");
    }

    protected static KeyCloakToken adminToken() {
        return getToken("admin", "123456");
    }

    protected static KeyCloakToken userToken() {
        return getToken("user", "123456");
    }

    protected static KeyCloakToken getToken(String username, String password) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put("grant_type", Collections.singletonList("password"));
        map.put("client_id", Collections.singletonList("banker-client"));
        map.put("client_secret", Collections.singletonList(""));
        map.put("username", Collections.singletonList(username));
        map.put("password", Collections.singletonList(password));
        KeyCloakToken token =
                restTemplate.postForObject(
                        authServerUrl , new HttpEntity<>(map, httpHeaders), KeyCloakToken.class);

        assert token != null;
        return token;
    }

}
